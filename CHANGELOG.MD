0.1.4
- add axios api call (/api/getcsv)

0.1.3
- move app into crboard

0.1.2

- add base url customize and publicpath in build

0.1.1

- use csvtojson instead of @nuxt/content
- file name must be static (cr_report.csv) 