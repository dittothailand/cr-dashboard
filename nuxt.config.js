let BASE_URL = process.env.BASE_URL

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  server: {
    port: 8080 // default: 3000
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    '@nuxtjs/axios'
  ],

  axios: {
    proxy: true // Can be also an object with default options
  },

  proxy: {
    '/api/': { target: 'http://localhost:3000' }
    // '/api/': { target: 'http://localhost:3000', pathRewrite: {'^/api/': ''} }
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    publicPath: `${BASE_URL}/_nuxt/`
  },

  generate: {
    ignore: [
      '.nuxt', // buildDir
      'static', // dir.static
      'dist', // generate.dir
      'node_modules',
      '.**/*',
      '.*',
      'README.md'
    ]
  }
}
