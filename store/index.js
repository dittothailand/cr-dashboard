export const state = () => ({
    loginname: '',
    companyname: '',
    companylogopath: ''
 })

export const getters = {
    getLogo: (state) => {
        return state.companylogopath
    }
} 
  
export const mutations = {
    LOAD_ADMIN_NAME(state, i) {
    state.loginname = i;
    },
    LOAD_COMP_NAME(state, i) {
    state.companyname = i;
    },
    LOAD_COMP_PATH(state, i) {
    state.companylogopath = i;
    },
}

export const actions = {
} 
  